# OpenCV Demos

Diferentes demos con OpenCV

* 0000 Base.py: plantilla base para experimentar con OpenCV
* 0010 Bordes-Canny: detección de bordes utilizando Canny
* 0011 Bordes-Laplace.py: detección de bordes utilizando Laplace
* 0012 Bordes-Contours.py: detección de bordes utilizando findContours()
* 0020 Detecta-Circulos: detección de zonas circulares
* 0030 Tracking-HSV.py: seguimiento de objetos por color
* 0040 Dual-Camera: despliegue simultaneo de dos camaras
* Video_Send.py: transmite video a un servidor MQTT
