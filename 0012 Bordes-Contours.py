#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import numpy as np
import sys

G_IS_CV3 = ( cv2.__version__[0] == '3' )
if( G_IS_CV3 ):
    G_IMREAD_COLOR = cv2.IMREAD_COLOR
    G_COLOR_BGR2RGB = cv2.COLOR_BGR2RGB
    G_IMWRITE_JPEG_QUALITY = cv2.IMWRITE_JPEG_QUALITY
    G_CAP_PROP_FRAME_HEIGHT = cv2.CAP_PROP_FRAME_HEIGHT
    G_CAP_PROP_FRAME_WIDTH = cv2.CAP_PROP_FRAME_WIDTH
    G_WINDOW_AUTOSIZE = cv2.WINDOW_AUTOSIZE
else:
    G_IMREAD_COLOR = cv2.CV_LOAD_IMAGE_COLOR
    G_COLOR_BGR2RGB = cv2.cv.CV_BGR2RGB
    G_IMWRITE_JPEG_QUALITY = cv2.cv.CV_IMWRITE_JPEG_QUALITY
    G_CAP_PROP_FRAME_HEIGHT = cv2.cv.CV_CAP_PROP_FRAME_HEIGHT
    G_CAP_PROP_FRAME_WIDTH = cv2.cv.CV_CAP_PROP_FRAME_WIDTH
    G_WINDOW_AUTOSIZE = cv2.CV_WINDOW_AUTOSIZE

_tbData = {
    'Low': 0,
    'High' : 255
    }


def procesaCuadro( img, imgH, imgW ):
    global _tbData

    # construimos una nueva imagen para el resultado
    frame = img.copy()
    imgW2 = int( imgW/2 )

    # la mitad derecha
    mitad = img[:,imgW2:]
    contornos = ~np.zeros( ( imgH, imgW2, 3 ), np.uint8 )

    # necesario
    gray = cv2.cvtColor( mitad, cv2.COLOR_BGR2GRAY )

    # contornos
    ret,thresh = cv2.threshold( gray, _tbData['Low'], _tbData['High'], 0 )
    if( G_IS_CV3 ):
        _,contours, hierarchy = cv2.findContours( thresh, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE )
    else:
        contours, hierarchy = cv2.findContours( thresh, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE )
    cv2.drawContours( contornos, contours, -1, ( 0, 0, 0), 2 )

    # final
    final = cv2.GaussianBlur( contornos, ( 3, 3 ), 0 )

    # reemplazamos la mitad procesada
    frame[:,imgW2:] = final
    return frame

def tbLow( nivel ):
    global _tbData

    _tbData['Low'] = nivel

def tbHigh( nivel ):
    global _tbData

    _tbData['High'] = nivel

def main( args ):
    # ticks por segundos
    tps = cv2.getTickFrequency()

    # abrimos dispositivo de captura
    device = 0
    cap = cv2.VideoCapture( device )

    # establecemos dimensiones
    imgH,imgW = 240, 320
    cap.set( G_CAP_PROP_FRAME_HEIGHT, imgH )
    cap.set( G_CAP_PROP_FRAME_WIDTH, imgW )

    # preparamos la ventana
    winName = 'Frames'
    cv2.namedWindow( winName, G_WINDOW_AUTOSIZE )
    cv2.createTrackbar('TH Low', winName, 0, 255, tbLow )
    cv2.createTrackbar('TH High', winName, 255, 255, tbHigh )

    # procesamos hasta que recibamos ESC
    t1=cv2.getTickCount()
    while True:
        # capturamos un cuadro
        ret, img = cap.read()
        if( ret ):
            # procesamos el cuadro
            img = cv2.flip( img,1 )
            frame = procesaCuadro( img, imgH, imgW )

            # calculamos FPS y mostramos el frame procesado
            t2 = cv2.getTickCount()
            cv2.putText( frame, "%04.2f FPS" % ( 1/( ( t2-t1 )/tps ) ), ( 10, imgH-10 ), cv2.FONT_HERSHEY_SIMPLEX, 1, ( 255, 255, 255 ) )
            cv2.imshow( winName, frame )
            t1 = t2

        # verificamos si se presiona ESC
        if( cv2.waitKey( 5 ) == 27 ):
            break

    cap.release()
    cv2.destroyAllWindows()


# Show time
if __name__ == '__main__':
    ret = main( sys.argv )
    sys.exit( ret )
