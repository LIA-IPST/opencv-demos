#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import numpy as np
import sys

G_IS_CV3 = ( cv2.__version__[0] == '3' )
if( G_IS_CV3 ):
    G_IMREAD_COLOR = cv2.IMREAD_COLOR
    G_COLOR_BGR2RGB = cv2.COLOR_BGR2RGB
    G_IMWRITE_JPEG_QUALITY = cv2.IMWRITE_JPEG_QUALITY
    G_CAP_PROP_FRAME_HEIGHT = cv2.CAP_PROP_FRAME_HEIGHT
    G_CAP_PROP_FRAME_WIDTH = cv2.CAP_PROP_FRAME_WIDTH
    G_WINDOW_AUTOSIZE = cv2.WINDOW_AUTOSIZE
    G_HOUGH_GRADIENT = cv2.HOUGH_GRADIENT
else:
    G_IMREAD_COLOR = cv2.CV_LOAD_IMAGE_COLOR
    G_COLOR_BGR2RGB = cv2.cv.CV_BGR2RGB
    G_IMWRITE_JPEG_QUALITY = cv2.cv.CV_IMWRITE_JPEG_QUALITY
    G_CAP_PROP_FRAME_HEIGHT = cv2.cv.CV_CAP_PROP_FRAME_HEIGHT
    G_CAP_PROP_FRAME_WIDTH = cv2.cv.CV_CAP_PROP_FRAME_WIDTH
    G_WINDOW_AUTOSIZE = cv2.CV_WINDOW_AUTOSIZE
    G_HOUGH_GRADIENT = cv2.cv.CV_HOUGH_GRADIENT

_h, _s, _v = 0, 0, 0


def procesaCuadro( img_1, img_2, imgH, imgW, celdas ):
    global _blur

    frame = img_1

    # trazamos la grilla
    ch, cw = int( imgH/celdas ), int( imgW/celdas )
    #for y in range(celdas):
    #    for x in range(celdas):
    #        cv2.rectangle( frame, ( x*cw,y*ch ), ( x*cw+cw,y*ch+ch ), ( 0,0,0 ), 1 )

    # necesario
    img_1 = cv2.cvtColor( img_1, cv2.COLOR_BGR2GRAY )
    img_1 = cv2.GaussianBlur( img_1, ( 5, 5 ), 0 )
    img_2 = cv2.cvtColor( img_2, cv2.COLOR_BGR2GRAY )
    img_2 = cv2.GaussianBlur( img_2, ( 5, 5 ), 0 )
    diff = cv2.absdiff( img_2, img_1 )

    # obtenemos los circulos y los trazamos
    circles = cv2.HoughCircles( diff, G_HOUGH_GRADIENT, 2, imgH, 255, 100  )
    if( circles is not None ):
        circles = np.uint16( np.around( circles ) )
        for i in circles[0,:]:
            cv2.circle( frame,( i[0], i[1]), i[2], (0, 255 ,0 ), 1 )   # draw the outer circle
            cv2.circle( frame,( i[0], i[1]), 2, ( 0, 0, 255 ), 3 )     # draw the center of the circle

    return frame

def main( args ):
    # ticks por segundos
    tps = cv2.getTickFrequency()

    # abrimos dispositivo de captura y obtenemos dimensiones
    device = 0
    cap = cv2.VideoCapture( device )
    ret,img = cap.read()
    imgH,imgW,channels = img.shape

    size = ( int( imgW/1 ), int( imgH/1 ) )
    img = cv2.resize(img, size )
    imgH, imgW, channels = img.shape

    # preparamos la ventana
    winName = 'Frames'
    cv2.namedWindow( winName, G_WINDOW_AUTOSIZE )

    # celdas
    celdas = 12

    # necesitamos 1 cuadro previo
    ret, img_1 = cap.read()
    img_1 = cv2.resize( img_1, size )

    # procesamos hasta que recibamos ESC
    t1 = cv2.getTickCount()
    while True:
        # capturamos un cuadro y lo procesamos
        ret, img_2 = cap.read()
        img_2 = cv2.resize( img_2, size )
        if( ret ):
            # procesamos el cuadro
            frame = procesaCuadro( img_1, img_2, imgH, imgW, celdas )
            frame = cv2.flip( frame,1 )

            # calculamos FPS y mostramos el frame procesado
            t2 = cv2.getTickCount()
            cv2.putText( frame, "%04.2f FPS" % ( 1/( ( t2-t1 )/tps ) ), ( 10, imgH-10 ), cv2.FONT_HERSHEY_SIMPLEX, 1, ( 0, 0, 0 ) )
            cv2.imshow( winName, frame )
            t1 = t2

            # conservamos el cuadro
            img_1 = img_2

        # verificamos si se presiona ESC
        if( cv2.waitKey( 5 ) == 27 ):
            break

    cap.release()
    cv2.destroyAllWindows()


# Show time
if __name__ == '__main__':
    ret = main( sys.argv )
    sys.exit( ret )

