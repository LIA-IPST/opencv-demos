#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import numpy as np
import sys

G_IS_CV3 = ( cv2.__version__[0] == '3' )
if( G_IS_CV3 ):
    G_IMREAD_COLOR = cv2.IMREAD_COLOR
    G_COLOR_BGR2RGB = cv2.COLOR_BGR2RGB
    G_IMWRITE_JPEG_QUALITY = cv2.IMWRITE_JPEG_QUALITY
    G_CAP_PROP_FRAME_HEIGHT = cv2.CAP_PROP_FRAME_HEIGHT
    G_CAP_PROP_FRAME_WIDTH = cv2.CAP_PROP_FRAME_WIDTH
    G_WINDOW_AUTOSIZE = cv2.WINDOW_AUTOSIZE
else:
    G_IMREAD_COLOR = cv2.CV_LOAD_IMAGE_COLOR
    G_COLOR_BGR2RGB = cv2.cv.CV_BGR2RGB
    G_IMWRITE_JPEG_QUALITY = cv2.cv.CV_IMWRITE_JPEG_QUALITY
    G_CAP_PROP_FRAME_HEIGHT = cv2.cv.CV_CAP_PROP_FRAME_HEIGHT
    G_CAP_PROP_FRAME_WIDTH = cv2.cv.CV_CAP_PROP_FRAME_WIDTH
    G_WINDOW_AUTOSIZE = cv2.CV_WINDOW_AUTOSIZE


def procesaCuadro( img ):
    # necesario
    gray = cv2.cvtColor( img, cv2.COLOR_BGR2GRAY )
    blur = cv2.GaussianBlur( gray, ( 7, 7 ), 0 )

    # bordes
    canny = cv2.Canny( blur, 5, 200 )

    return cv2.cvtColor( ~canny, cv2.COLOR_GRAY2BGR )

def main( args ):
    # ticks por segundos
    tps = cv2.getTickFrequency()

    # abrimos dispositivo de captura y obtenemos dimensiones
    device1 = 0
    device2 = 2
    cap1 = cv2.VideoCapture( device1 )
    cap2 = cv2.VideoCapture( device2 )

    # Tamaños involucrados
    h1 = cap1.get( G_CAP_PROP_FRAME_HEIGHT )
    w1 = cap1.get( G_CAP_PROP_FRAME_WIDTH )
    h2 = cap2.get( G_CAP_PROP_FRAME_HEIGHT )
    w2 = cap2.get( G_CAP_PROP_FRAME_WIDTH )
    imgW = 640
    imgH = 480
    imgW2 = int( imgW/2 )
    imgH2 = int( imgH/2 )

    # preparamos la ventana
    winName = 'Frames'
    cv2.namedWindow( winName, G_WINDOW_AUTOSIZE )

    # procesamos hasta que recibamos ESC
    t1 = cv2.getTickCount()
    while True:
        # capturamos un cuadro y lo procesamos
        ret, img1 = cap1.read()
        ret, img2 = cap2.read()

        # procesamos el cuadro
        img1 = cv2.flip( img1, 1 )
        img1 = cv2.resize( img1, ( imgW2, imgH2 ) )
        frm1 = procesaCuadro( img1 )
        img2 = cv2.flip( img2, 1 )
        img2 = cv2.resize( img2, ( imgW2, imgH2 ) )
        frm2 = procesaCuadro( img2 )

        frame = np.zeros(( imgH, imgW, 3 ), np.uint8 )
        frame[0:imgH2, 0:imgW2] = img1
        frame[0:imgH2, imgW2:imgW] = frm1
        frame[imgH2:imgH, 0: imgW2] = img2
        frame[imgH2:imgH, imgW2:imgW] = frm2

        # calculamos FPS y mostramos el frame procesado
        t2=cv2.getTickCount()
        cv2.putText( frame, "%04.2f FPS" % ( 1/( ( t2-t1 )/tps ) ), ( 10, imgH-10 ), cv2.FONT_HERSHEY_SIMPLEX, 1, ( 255, 255, 255 ) )
        cv2.imshow( winName, frame )
        t1 = t2

        # verificamos si se presiona ESC
        if( cv2.waitKey( 5 ) == 27 ):
            break

    cap1.release()
    cap2.release()
    cv2.destroyAllWindows()


# Show time
if __name__ == '__main__':
    ret = main( sys.argv )
    sys.exit( ret )
