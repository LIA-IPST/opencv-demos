#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import numpy as np
import sys

G_IS_CV3 = ( cv2.__version__[0] == '3' )
if( G_IS_CV3 ):
    G_IMREAD_COLOR = cv2.IMREAD_COLOR
    G_COLOR_BGR2RGB = cv2.COLOR_BGR2RGB
    G_IMWRITE_JPEG_QUALITY = cv2.IMWRITE_JPEG_QUALITY
    G_CAP_PROP_FRAME_HEIGHT = cv2.CAP_PROP_FRAME_HEIGHT
    G_CAP_PROP_FRAME_WIDTH = cv2.CAP_PROP_FRAME_WIDTH
    G_WINDOW_AUTOSIZE = cv2.WINDOW_AUTOSIZE
else:
    G_IMREAD_COLOR = cv2.CV_LOAD_IMAGE_COLOR
    G_COLOR_BGR2RGB = cv2.cv.CV_BGR2RGB
    G_IMWRITE_JPEG_QUALITY = cv2.cv.CV_IMWRITE_JPEG_QUALITY
    G_CAP_PROP_FRAME_HEIGHT = cv2.cv.CV_CAP_PROP_FRAME_HEIGHT
    G_CAP_PROP_FRAME_WIDTH = cv2.cv.CV_CAP_PROP_FRAME_WIDTH
    G_WINDOW_AUTOSIZE = cv2.CV_WINDOW_AUTOSIZE

_tbData = {
    'H' : 0,
    'S' : 0,
    'V' : 0,
    'Area' : 300
    }


def procesaCuadro( img, imgH, imgW ):
    global _tbData

    # minimizamos ruido
    blur = cv2.GaussianBlur( img, ( 9, 9 ), 0 )

    # trabajaremos en el espacio hsv
    hsv = cv2.cvtColor( blur, cv2.COLOR_BGR2HSV )
    h,s,v = cv2.split( hsv )

    # ajustamos umbral acorde a los trackbar
    h = cv2.threshold( h, _tbData['H'], 255, cv2.THRESH_BINARY )[1]
    s = cv2.threshold( s, _tbData['S'], 255, cv2.THRESH_BINARY )[1]
    v = cv2.threshold( v, _tbData['V'], 255, cv2.THRESH_BINARY )[1]

    # mostramos las ventanas h,s,v
    cv2.imshow( 'H', h )
    cv2.imshow( 'S', s )
    cv2.imshow( 'V', v )

    # aplicamos umbral y otros filtros
    objs = h & s & v
    objs = cv2.erode( objs, None, iterations=5 )
    objs = cv2.dilate( objs, None, iterations=10 )
    canny = cv2.Canny( objs, 15, 15*3 )

    # buscamos y trazamos los contornos
    if( G_IS_CV3 ):
        _,contours,_ = cv2.findContours( canny, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE )
    else:
        contours = cv2.findContours( canny, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE )[0]
    objs = cv2.cvtColor( objs, cv2.COLOR_GRAY2BGR )
    cv2.drawContours( objs, contours, -1, ( 0, 0, 255 ), 2 )
    cv2.imshow( 'Mask', objs )

    # buscamos el contorno de mayor área
    contour = ( None, 0 )
    for cnt in contours:
        #moments = cv2.moments(cnt)
        #area = cv2.contourArea(cnt)
        #perimeter = cv2.arcLength(cnt,True)
        #approx = cv2.approxPolyDP(cnt,0.1*cv2.arcLength(cnt,True),True)
        #hull = cv2.convexHull(cnt)
        area = cv2.contourArea( cnt )
        if( area>contour[1] and area>_tbData['Area'] ):
            contour = ( cnt, area )

    # dibujamos el mayor de los contornos
    frame = img.copy()
    if( contour[0] is not None ):
        ( x,y ), radius = cv2.minEnclosingCircle( contour[0] )
        center = ( int( x ), int( y ) )
        radius = int( radius )
        cv2.circle( frame,center, radius, ( 0, 255, 0 ), 1 )
        cv2.circle( frame, center, 2, ( 0, 0, 255 ), 2 )
        return frame, center
    else:
        return frame, None

def tbHUmbral( nivel ):
    global _tbData

    _tbData['H'] = nivel

def tbSUmbral( nivel ):
    global _tbData

    _tbData['S'] = nivel

def tbVUmbral( nivel ):
    global _tbData

    _tbData['V'] = nivel

def tbArea( nivel ):
    global _tbData

    _tbData['Area'] = nivel

def main( args ):
    # ticks por segundos
    tps = cv2.getTickFrequency()

    # abrimos dispositivo de captura
    device = 0
    cap = cv2.VideoCapture( device )

    # establecemos dimensiones
    imgH,imgW = 240, 320
    cap.set( G_CAP_PROP_FRAME_HEIGHT, imgH )
    cap.set( G_CAP_PROP_FRAME_WIDTH, imgW )

    # preparamos las ventanas
    cv2.namedWindow( 'Frames', G_WINDOW_AUTOSIZE )
    cv2.namedWindow( 'H', G_WINDOW_AUTOSIZE )
    cv2.namedWindow( 'S', G_WINDOW_AUTOSIZE )
    cv2.namedWindow( 'V', G_WINDOW_AUTOSIZE )
    cv2.namedWindow( 'Mask', G_WINDOW_AUTOSIZE )

    # preparamos las barras de umbral
    cv2.createTrackbar( 'H Mask', 'H', 0, 255, tbHUmbral )
    cv2.createTrackbar( 'S Mask', 'S', 0, 255, tbSUmbral )
    cv2.createTrackbar( 'V Mask', 'V', 0, 255, tbVUmbral )
    cv2.createTrackbar( 'Area', 'Frames', 300, 5000, tbArea )

    # init interno
    zonaL = int( imgW/3 )
    zonaR = int( imgW/3*2 )

    # abrimos la camara
    capDV = cv2.VideoCapture(0)

    # procesamos hasta que recibamos ESC
    t1 = cv2.getTickCount()
    while True:
        # capturamos un cuadro
        ret, img = cap.read()
        if( ret ):
            # procesamos el cuadro
            img = cv2.flip( img, 1 )
            frame, center = procesaCuadro( img, imgH, imgW )

            # añadimos lineas de zonas
            cv2.line( frame, ( zonaL, 0 ), ( zonaL, imgH ), ( 0, 0, 0 ), 1 )
            cv2.line( frame, ( zonaR, 0 ), ( zonaR, imgH ), ( 0, 0, 0 ), 1 )

            # calculamos FPS y mostramos el frame procesado
            t2 = cv2.getTickCount()
            cv2.putText( frame, "%04.2f FPS" % ( 1/( ( t2-t1 )/tps ) ), ( 10, imgH-10 ), cv2.FONT_HERSHEY_SIMPLEX, 1, ( 0, 0, 0 ) )
            cv2.imshow( 'Frames', frame )
            t1 = t2

        # verificamos si se presiona ESC
        if( cv2.waitKey( 5 ) == 27 ):
            break

    cap.release()
    cv2.destroyAllWindows()


# Show time
if __name__ == '__main__':
    ret = main( sys.argv )
    sys.exit( ret )
